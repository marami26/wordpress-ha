echo "Creating an AMI for $1"
IMAGE_ID=`aws ec2 create-image --instance-id $1 --name "My server5" --description "An AMI for my server" | jq -r '.ImageId'`

echo "Creating launch config"
aws autoscaling create-launch-configuration --launch-configuration-name my-launch-config --key-name bastion --image-id $IMAGE_ID --instance-type t2.micro --security-groups sg-062d17945dc15fa5d